# 模拟停车场收费系统

#### Description
基于ARM，实现停车场收费系统。
入场模块：
1.车主刷卡进场时，蜂鸣器响应一声，若已经刷过卡，再次刷卡蜂鸣器连续响应。
2.采集车主的车牌图像，调用车牌识别的API，接受车牌文本。
3.将车牌文本存入系统的数据库中，且发收给语音合成服务器，播放“欢迎 XXXXXX 入场”。
出场模块：
1.车主刷卡出场时，蜂鸣响应一声，若已经刷卡，刷卡无响应。
2.获取当前时间，与当前车主入场的时间相比进行计费，发出计费文本给语音合成服务器，播放“停车时长XXX，收费XXXX”
3.删除当前车主存入数据库的数据。

PS：文本语音合成使用了科大讯飞的离线源码包，稍作修改为服务器。车牌识别调用了阿里云上的API，关键在于HTTP报文的编写。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)