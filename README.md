# 模拟停车场收费系统

#### 介绍
基于ARM，实现停车场收费系统。
入场模块：
1.车主刷卡进场时，蜂鸣器响应一声，若已经刷过卡，再次刷卡蜂鸣器连续响应。
2.采集车主的车牌图像，调用车牌识别的API，接受车牌文本。
3.将车牌文本存入系统的数据库中，且发收给语音合成服务器，播放“欢迎 XXXXXX 入场”。
出场模块：
1.车主刷卡出场时，蜂鸣响应一声，若已经刷卡，刷卡无响应。
2.获取当前时间，与当前车主入场的时间相比进行计费，发出计费文本给语音合成服务器，播放“停车时长XXX，收费XXXX”
3.删除当前车主存入数据库的数据。

PS：文本语音合成使用了科大讯飞的离线源码包，稍作修改为服务器。车牌识别调用了阿里云上的API，关键在于HTTP报文的编写。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)