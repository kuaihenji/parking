#ifndef _CARREQ_H_
#define _CARREQ_H_
#include<stdio.h>
#include<string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include<stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "cJSON.h"
#include<stdbool.h>
#define MAXSIZE 102400



char *http_request(char *base64,long len);
int get_size(char *head);
char *get_cardnumber(char *json);
bool ask4http(int sockfd);
char *get_json(int sockfd);


#endif
