
#ifndef __CAMINFO_H
#define __CAMINFO_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>

#include <linux/fb.h>
#include <linux/videodev2.h>
#include <linux/input.h>
#include <sys/ioctl.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <setjmp.h>

extern char formats[5][16];
extern struct v4l2_fmtdesc fmtdesc;
extern struct v4l2_format  fmt;
extern struct v4l2_capability cap;

void get_caminfo(int camfd);
void get_camfmt(int camfd);
void get_camcap(int camfd);
void set_camfmt(int camfd, char *pixfmt);

#endif
