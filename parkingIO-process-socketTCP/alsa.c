#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "fifo.h"
#include "common.h"

#define WAV "/tmp/a.wav"

void recv_wav(int fd)
{
	FILE *fp = fopen(WAV, "w");

	char buf[1024];
	bzero(buf, 1024);
	read(fd,buf,1024);
	long len= atol(buf);
	printf("len:%ld\n",len);
	
	while(len>0)
	{
		// 读取从Ubuntu发来的音频数据
		bzero(buf, 1024);
		int n = read(fd, buf, 1024);
		if(n <= 0)
			break;

		fwrite(buf, n, 1, fp);
		len -= n;
	}
	printf("tui\n");
	fclose(fp);
}

void playback(void)
{
	system("aplay /tmp/a.wav");
}

int main(int argc, char **argv)
{
	// 1，准备好TCP套接字，和服务端的地址
	int fd = Socket(AF_INET, SOCK_STREAM, 0);
	int fifo_alsa= open(SQLite_ALSA,O_RDWR);
	
	struct sockaddr_in addr;
	socklen_t len = sizeof(addr);
	bzero(&addr, len);

	addr.sin_family      = AF_INET;
	addr.sin_addr.s_addr = inet_addr(argv[1]);
	addr.sin_port        = htons(atoi(argv[2]));

	// 2，试图连接Ubuntu上的语音合成引擎
	Connect(fd, (struct sockaddr *)&addr, len);

	char msg[200];
	while(1)
	{
		// 3，从键盘获取文本
		bzero(msg, 200);
		//fgets(msg, 200, stdin);
		read(fifo_alsa,msg,200);
		printf("msg:%s\n",msg);
		// 4，将文本发送给语音合成引擎
		Write(fd, msg, strlen(msg));

		// 5，等待返回的语音数据，并播放出来
		recv_wav(fd);
		playback();
	}
}
